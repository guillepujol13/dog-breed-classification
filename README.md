# Dog Breed Classification

This is our Deep learning project for Pompeu Fabra's University assigment.

At first, we got an accuracy of 90%, but then we realised that it wasn’t likely to get those values with the number of Layers and Epochs that we had defined. 
Therefore, we analysed our use of the Dataset and soon found that we had been using the same images for both our train and our test. 
To solve this, we used a random_split function to separate the data between our test and train categories; the results obtained were more coherent with our goals and expectations for our model at this stage of the project.

This commit was done to deliver the final project of the subject, regardless of its optimal performance. We will continue to work on our own with the model to improve it, since, if perfected, we expect for it to have promising results.

