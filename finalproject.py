# -*- coding: utf-8 -*-
"""

Guillem Pujol Martínez
Gerard Garcia Moreno

"""

import torch
import torchvision
import os
from skimage import io, transform
import torchvision.transforms as transforms
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from PIL import Image
from pathlib import Path
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import torch.optim as optim
from torch.autograd import Variable
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F

"""The output of torchvision datasets are PILImage images of range [0, 1].
We transform them to Tensors of normalized range [-1, 1].
DOGOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOS
"""
breeds_list = "affenpinscher,afghan_hound,african_hunting_dog,airedale,american_staffordshire_terrier,appenzeller,australian_terrier,basenji,basset,beagle,bedlington_terrier,bernese_mountain_dog,black-and-tan_coonhound,blenheim_spaniel,bloodhound,bluetick,border_collie,border_terrier,borzoi,boston_bull,bouvier_des_flandres,boxer,brabancon_griffon,briard,brittany_spaniel,bull_mastiff,cairn,cardigan,chesapeake_bay_retriever,chihuahua,chow,clumber,cocker_spaniel,collie,curly-coated_retriever,dandie_dinmont,dhole,dingo,doberman,english_foxhound,english_setter,english_springer,entlebucher,eskimo_dog,flat-coated_retriever,french_bulldog,german_shepherd,german_short-haired_pointer,giant_schnauzer,golden_retriever,gordon_setter,great_dane,great_pyrenees,greater_swiss_mountain_dog,groenendael,ibizan_hound,irish_setter,irish_terrier,irish_water_spaniel,irish_wolfhound,italian_greyhound,japanese_spaniel,keeshond,kelpie,kerry_blue_terrier,komondor,kuvasz,labrador_retriever,lakeland_terrier,leonberg,lhasa,malamute,malinois,maltese_dog,mexican_hairless,miniature_pinscher,miniature_poodle,miniature_schnauzer,newfoundland,norfolk_terrier,norwegian_elkhound,norwich_terrier,old_english_sheepdog,otterhound,papillon,pekinese,pembroke,pomeranian,pug,redbone,rhodesian_ridgeback,rottweiler,saint_bernard,saluki,samoyed,schipperke,scotch_terrier,scottish_deerhound,sealyham_terrier,shetland_sheepdog,shih-tzu,siberian_husky,silky_terrier,soft-coated_wheaten_terrier,staffordshire_bullterrier,standard_poodle,standard_schnauzer,sussex_spaniel,tibetan_mastiff,tibetan_terrier,toy_poodle,toy_terrier,vizsla,walker_hound,weimaraner,welsh_springer_spaniel,west_highland_white_terrier,whippet,wire-haired_fox_terrier,yorkshire_terrier"
breeds_list = breeds_list.split(',')

learning_rate = .001
n_epochs = 50
batch_size = 10
imageSize = 100

PATH = myDrive+"Model/model.pt"
PATH2 = myDrive+"Model/modelMoreEpochs.pt"

# turns torch tensor into pil image and shows it
def showTorchImage(image):
	image = image.mul(255.0)
	image = transforms.ToPILImage()(image)
	image.show()

class DoggoDataset(Dataset):
	# Dataset of dog photos and their breeds (training examples)

	def __init__(self, csv_file, root_dir):
		# Arguments:
			# csv_file (string): Path to csv file w/ annotations
			# root_dir (string): Directory w/ all images
		self.breeds_frame = pd.read_csv(csv_file)
		self.root_dir = root_dir

	def __len__(self):
		return len(self.breeds_frame)

	# returns a dict {'breed': breed, 'image': image}
	def __getitem__(self, idx):
		# idx: image index (out off 12000-ish)		
		breed = self.breeds_frame.iloc[idx, 1]		
		breed = breeds_list.index(breed)
		img_id = self.breeds_frame.iloc[idx, 0]
		img_name = os.path.join(self.root_dir, 'train/' + img_id + '.jpg')
		image = io.imread(img_name) # image is a numpy array: <width x height x color>


		# transforms every image to be 200x200x3
		image = Image.fromarray(image, 'RGB') # numpy array -> PIL image
		image = transforms.Resize(imageSize)(image)
		image = transforms.RandomCrop(imageSize)(image)

		# PIL image -> (normalized) torch FloatTensor
		image = transforms.ToTensor()(image).float().div(255.0)
		
		# if self.transform is not None : 
		# 	image = self.transform(image)

		sample = (image, breed)
		return sample

class SimpleConv(torch.nn.Module):

	def __init__(self):
		super(SimpleConv, self).__init__()
		self.layer1 = torch.nn.Sequential(
		  	torch.nn.Conv2d(3, 40, 5, 2),
		    torch.nn.BatchNorm2d(40),
		  	torch.nn.ReLU())
		self.layer2 = torch.nn.Sequential(
			torch.nn.Conv2d(40, 80, 5, 2),
			torch.nn.BatchNorm2d(80),
			torch.nn.ReLU())
		self.layer3 = torch.nn.Sequential(
			torch.nn.Conv2d(80, 160, 5),
			torch.nn.BatchNorm2d(160),
			torch.nn.ReLU()) 
		self.layer4 = torch.nn.Sequential(
			torch.nn.Conv2d(160, 240, 5),
			torch.nn.BatchNorm2d(240),
			torch.nn.ReLU()) 
		self.pool = torch.nn.MaxPool2d(2, 2)

		self.fc1 = torch.nn.Linear(240, 120)

	def forward(self, x):
		
		x = self.layer1(x)
		x = self.layer2(x)
		x = self.pool(x)
		x = self.layer3(x)
		x = self.layer4(x)
		x = self.pool(x)
		x = x.view(x.shape[0], -1)
		x = self.fc1(x)
		return x

#-----------------------------------------------------

dataset = DoggoDataset(csv_file=myDrive+'Doggos/labels.csv', root_dir=myDrive+'Doggos/')

train_size = int(0.8 * len(dataset))
test_size = len(dataset) - train_size

# train_loader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
train_set, test_set = torch.utils.data.random_split(dataset, [train_size, test_size])

train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=True)
test_loader = DataLoader(test_set, batch_size=batch_size, shuffle=True)


# images, labels = dataiter.next()
# print(labels[1])
# #showTorchImage(images[1])
# print(breeds_list[labels[1]])


model = SimpleConv()
criterion = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

#showTorchImage(train_set[234][0]) # image check works

if(True):
	model.load_state_dict(torch.load(PATH2))

if(False):#Training
	total_step = len(train_loader)
	# training network
	for epoch in range(n_epochs):
		for i, samples in enumerate(train_loader):
			images, breeds = samples
			outputs = model.forward(images)
			# Forward pass
			loss = criterion(outputs, breeds)
			# Backward and optimize
			optimizer.zero_grad()
			loss.backward()
			optimizer.step()
			if (i+1) % 100 == 0:
				print("Breeds: {}".format(breeds_list[breeds[2]]))
				print("Outputs: {}".format(outputs[2]))
				print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'.format(epoch+1, n_epochs, i+1, total_step, loss.item()))
		torch.save(model.state_dict(), PATH2)

	

if(True):#Testing
	correct = 0
	total = 0
	with torch.no_grad():
		for data in test_loader:
			images, labels = data
			outputs = model.forward(images)
			_, predicted = torch.max(outputs.data, 1)
			total += labels.size(0)
			correct += (predicted == labels).sum().item()
			
	accuracy = (100 * correct / total)
	print('Accuracy of the network on the {} test images: {}%'.format(total, accuracy))

